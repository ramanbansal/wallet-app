import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CardService {

  constructor(
    private readonly snackBar: MatSnackBar,
    private httpClient: HttpClient
  ) { }

  getCardListApi() {
    const headers = {
      'Content-Type': 'application/json'
    }
    return this.httpClient.get(`${environment.url}/get-list`, { headers: headers });
  }

  saveCardApi(body) {
    const headers = {
      'Content-Type': 'application/json'
    }
    return this.httpClient.post(`${environment.url}/add-card`, body, { headers: headers });
  }

  deleteCardApi(cardNo) {
    const headers = {
      'Content-Type': 'application/json'
    }
    return this.httpClient.get(`${environment.url}/delete-card?cardNo=${cardNo}`, { headers: headers });
  }

  showSnackBar(msg: string): void {
    const WAITTIME = 4000;
    this.snackBar.open(msg, 'Close', {
      duration: WAITTIME,
    });
  }
}
