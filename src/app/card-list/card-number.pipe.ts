import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cardNumber'
})
export class CardNumberPipe implements PipeTransform {

  transform(value, type): String {
    if (!type) return;
    let num1 = value.substring(0, 4);
    let num2
    if (type == 'american') {
      num2 = value.substring(value.length - 5, value.length);
      return `${num1} XXXXXX ${num2}`;
    } else {
      num2 = value.substring(value.length - 4, value.length);
      return `${num1} XXXX XXXX ${num2}`;
    }
  }

}
