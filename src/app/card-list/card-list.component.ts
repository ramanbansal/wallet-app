import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddCardComponent } from '../add-card/add-card.component';
import { ConfirmBoxComponent } from '../confirm-box/confirm-box.component';
import { CardService } from './card.service';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss']
})
export class CardListComponent implements OnInit {
  cardList = [];
  constructor(
    private dialog: MatDialog,
    private cardService: CardService
  ) {
    this.getCardList();
  }

  ngOnInit(): void {
  }

  getCardList() {
    this.cardService.getCardListApi().subscribe((res) => {
      if (res && res['code'] == 'success') {
        this.cardList = res['data'];
      }
    })
  }

  deleteCard(card, index) {
    const dialogRef = this.dialog.open(ConfirmBoxComponent, {
      maxWidth: 'inherit',
      width: '30vw',
      data: {
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteCall(card, index)
      }
    });
  }


  deleteCall(card, index) {
    this.cardService.deleteCardApi(card.cardNo).subscribe((res) => {
      if (res && res['code'] == 'success') {
        this.cardList.splice(index, 1);
        this.cardService.showSnackBar('Card deleted successfully');
      }
    })
  }

  addCard() {
    const dialogRef = this.dialog.open(AddCardComponent, {
      maxWidth: 'inherit',
      width: '30vw',
      data: {
        cardList: this.cardList
      }
    });
    dialogRef.afterClosed().subscribe(result => {

    });
  }


}
