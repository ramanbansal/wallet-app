import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
  name: 'chipFilter'
})
export class ChipFilterPipe implements PipeTransform {

  transform(data, query) {
    if (data.length == 0 || !query) return;
    let filteredData = [];
    for (let i = 0; i < query.length; i++) {
      if (!query[i].includes('=')) {
        continue;
      }
      let splitValues = query[i].split('=');
      let key = splitValues[0].trim();
      let value = splitValues[1].trim();

      if (filteredData.length > 0) {
        const filterData = data.filter(item => {
          return this.checkInside(item[key], value);
        });
        filteredData.push(...filterData);
      } else {
        filteredData = data.filter(item => {
          return this.checkInside(item[key], value);
        });
      }
    }
    return _.uniqWith(filteredData, _.isEqual);
  }

  checkInside(item, value) {
    if (typeof (item) !== 'object') {
      if (item === undefined || !item.includes(value)) return false;
    } else {
      let ele = item.find(ele => ele['name'].includes(value));
      if (!ele) {
        return false;
      }
    }
    return true;
  }

}
