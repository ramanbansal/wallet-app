import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteFilterComponent } from './autocomplete-filter.component';
import { ChipFilterPipe } from './pipes/chip-filter.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';



@NgModule({
  declarations: [
    AutocompleteFilterComponent,
    ChipFilterPipe
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule
  ],
  exports: [AutocompleteFilterComponent,
    ChipFilterPipe]
})
export class AutocompleteFilterModule { }
