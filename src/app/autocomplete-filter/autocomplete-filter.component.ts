import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

export interface autoCompleteGroup {
  groupName: string;
  list: string[];
}

export const _filter = (opt: string[], value: string): string[] => {
  const filterValue = value.toLowerCase();

  return opt.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
};

@Component({
  selector: 'vcp-autocomplete-filter',
  templateUrl: './autocomplete-filter.component.html',
  styleUrls: ['./autocomplete-filter.component.scss']
})
export class AutocompleteFilterComponent implements OnInit {
  @Input('data') set getData(event) {
    this.data = event;
  };
  data = [];
  @Input() filterConfig;
  @Output() action = new EventEmitter();
  filteredOptions: Observable<autoCompleteGroup[]>;
  autoForm: FormGroup = this._formBuilder.group({
    autoGroup: '',
  });
  propertyList = []
  chipValue = null;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  autoCompleteList: autoCompleteGroup[] = [
    {
      groupName: 'Properties',
      list: [],
    }
  ];
  keys = [];
  values = [];
  operatorList = {
    groupName: 'Operators',
    list: ['OR'],
  };
  readonly separatorKeysCodes: number[] = [];
  stateGroupOptions: Observable<autoCompleteGroup[]>;
  searchList = [];
  constructor(
    private _formBuilder: FormBuilder
  ) { }

  addData(data) {
    const index = this.autoCompleteList.findIndex(ele => ele.groupName.toLowerCase() === 'properties');
    this.autoCompleteList[index].list = data;
  }

  initializePropertyList() {
    // -----------------get defined properities ----------------
    // let propertyList = ['name', 'status', 'permissions', 'type'];
    if (this.data && this.data.length > 0) {
      let properties = Object.keys(this.data[0]);
      this.propertyList = [];
      properties.forEach(property => {
        let ele = this.filterConfig.propertyList.find(ele1 => ele1 === property);
        if (ele) {
          this.propertyList.push(ele);
        }
      })
      this.addData(this.propertyList);
    }
  }

  ngOnInit(): void {
    // if (this.data && this.data.length > 0) {
    //   const data = Object.keys(this.data[0]);
    //   this.addData(data);
    // }
    this.initializePropertyList();
    this.filteredOptions = this.autoForm.get('autoGroup')!.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private _filter(value) {
    let filterValue;
    if (value.includes('=')) {
      filterValue = value.split('=')[1].trim().toLowerCase();
    } else {
      filterValue = value.toLowerCase().trim();
    }
    if (filterValue) {
      return this.autoCompleteList.map(group => ({ groupName: group.groupName, list: _filter(group.list, filterValue) }))
        .filter(group => group.list.length > 0);
    }
    return this.autoCompleteList;
  }

  getChipValue(event: Event) {
    // when i type in input it calls------------------------------
    const value = (event.target as HTMLInputElement).value;
    this.chipValue = value;
    this.checkSearchListOperator();
    this.getAutoCompleteList(value);
  }

  checkSearchListOperator() {
    if (this.searchList.length > 0) {
      let query = this.searchList[this.searchList.length - 1];
      if (query.includes('=')) {
        this.addOperators();
      } else {
        this.removeOperators();
      }
    }
  }

  checkComma(columnName) {
    let ele = this.data.find(ele => ele[columnName].includes(','));
    return ele ? true : false;
  }

  getCommavalues(columnName) {
    let allValues = [];
    this.data.forEach((ele) => {
      if (ele[columnName].includes(',')) {
        let splitValue = ele[columnName].split(',');
        splitValue.forEach((value) => {
          allValues.push(value.trim());
        });
      } else {
        allValues.push(ele[columnName]);
      }
    })
    return allValues;
  }

  checkColumnData(columnName) {
    if (this.data[0].hasOwnProperty(columnName)) {
      let data = [];
      let allValues = [];
      if (typeof (this.data[0][columnName]) !== 'object') {
        if (this.checkComma(columnName)) {
          allValues = this.getCommavalues(columnName);
        } else {
          allValues = this.data.map((ele) => ele[columnName]);
        }
      } else {
        //----------------only names value fetch from array of objects---------
        this.data.forEach(ele => {
          let result = ele[columnName].map(ele1 => ele1.name);
          allValues.push(...result);
        });
      }
      data = allValues.filter((v, i, a) => a.findIndex(t => (t === v)) === i);
      return data;
    }
    return [];
  }

  getAutoCompleteList(value) {
    if (value.includes('=')) {
      const columnName = value.split('=')[0].trim();
      const data = this.checkColumnData(columnName);
      this.addData(data);
      this.removeOperators();
    } else {
      // const data = Object.keys(this.data[0]);
      this.addData(this.propertyList);
    }
  }

  getAutoCompleteEvent(event: MatAutocompleteSelectedEvent): void {
    // --------------when i select option into autocomplete -------------------
    this.checkSearchListOperator();
    this.autoCompleteValue(event.option.viewValue);
  }

  checkChipValue() {
    if (this.chipValue && this.chipValue.includes('=')) {
      this.chipValue = `${this.chipValue.split('=')[0]} = `;
    }
  }

  checkAndOr(value) {
    //------------------  when i get or and and value in input ----------------------
    if (value.toLowerCase() === 'or' || value.toLowerCase() === 'and') {
      this.getSearchList(value);
      this.removeOperators();
      // const data = Object.keys(this.data[0]);
      this.addData(this.propertyList);
      this.openAutoCompletePanel();
      return true;
    }
    return false;
  }

  nullFormValue() {
    this.autoForm.get('autoGroup').setValue('');
  }

  autoCompleteValue(value) {
    this.nullFormValue();
    if (this.checkAndOr(value)) return;
    if (this.chipValue && this.chipValue.includes('=')) {
      this.checkChipValue();
      this.chipValue = this.chipValue ? `${this.chipValue} ${value}` : `${value} = `;
      this.getSearchList(this.chipValue);
      this.addOperators();
    } else {
      this.chipValue = `${value} = `;
      const data = this.checkColumnData(value);
      this.addData(data);
      this.removeOperators();
    }
    this.openAutoCompletePanel();
  }

  getSearchList(value) {
    this.searchList.push(value.trim());
    this.chipValue = '';
    this.action.emit({ searchList: this.searchList });
  }

  openAutoCompletePanel() {
    let ele = document.getElementById('chip') as HTMLInputElement;
    ele.blur();
    setTimeout(() => {
      ele.focus();
    }, 0)
    // this.inputAutoComplete.openPanel();
  }

  getChipEnterEvent(event: Event) {
    //----------------------------when user press enter key --------------------
    this.nullFormValue();
    const value = (event.target as HTMLInputElement).value;
    if (this.checkAndOr(value)) return;
    if (value && value.includes('=')) {
      const splitValue = value.split('=')[1].trim();
      if (!splitValue) return;
      this.getSearchList(value);
      this.addOperators();
      this.openAutoCompletePanel();
    }
  }

  add(event: MatChipInputEvent): void {
    // const input = event.input;
    // const value = event.value;

    // // Add our query
    // if ((value || '').trim()) {
    //   this.searchList.push(value.trim());
    // }

    // if (input) {
    //   input.value = '';
    // }

    // this.optionCtrl.setValue(null);

    // // Reset the input value
    // if (value.includes('=')) {
    //   let splitValue = value.trim().split('=');
    //   this.keys.push(splitValue[0]);
    //   this.values.push(splitValue[1]);
    //   this.searchFilter(this.keys, this.values);
    // }
  }

  remove(index): void {
    // const index = this.searchList.indexOf(searchValue);
    this.chipValue = '';
    if (index >= 0) {
      this.searchList.splice(index, 1);
      // this.keys.splice(index, 1);
      // this.values.splice(index, 1);
      // this.searchFilter(this.keys, this.values);
    }
    this.nullFormValue();
    this.checkSearchListOperator();
    if (this.searchList.length == 0) {
      this.removeOperators();
    }
    // const data = Object.keys(this.data[0]);
    this.addData(this.propertyList);
    this.action.emit({ searchList: this.searchList });
  }

  addOperators() {
    const ele = this.autoCompleteList.find(ele => ele.groupName.toLowerCase() == 'operators');
    if (!ele) {
      this.autoCompleteList.unshift(this.operatorList);
      // const data = Object.keys(this.data[0]);
      this.addData(this.propertyList);
    }
  }

  removeOperators() {
    const ele = this.autoCompleteList.find(ele => ele.groupName.toLowerCase() == 'operators');
    if (ele) {
      const index = this.autoCompleteList.findIndex(ele => ele.groupName.toLowerCase() == 'operators');
      this.autoCompleteList.splice(index, 1);

    }
  }

}
