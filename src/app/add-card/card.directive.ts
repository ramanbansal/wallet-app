import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appCard]'
})
export class CardDirective {

  constructor() { }
  @HostListener('input', ['$event'])

  onKeyDown(event: KeyboardEvent) {
    const input = event.target as HTMLInputElement;

    let trimmed = input.value.replace(/\s+/g, '');
    if (trimmed.length > 16) {
      trimmed = trimmed.substr(0, 16);
    }

    const partitions = trimmed.startsWith('34') || trimmed.startsWith('37')
      ? [4, 6, 5]
      : [4, 4, 4, 4];

    const numbers = [];
    let position = 0;
    partitions.forEach(partition => {
      const part = trimmed.substr(position, partition);
      if (part) numbers.push(part);
      position += partition;
    })
    input.value = numbers.join(' ');
  }
}
