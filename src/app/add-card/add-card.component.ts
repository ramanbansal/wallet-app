import { Component, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, map, first, catchError } from 'rxjs/operators';
import { CardService } from '../card-list/card.service';

@Component({
  selector: 'app-add-card',
  templateUrl: './add-card.component.html',
  styleUrls: ['./add-card.component.scss']
})
export class AddCardComponent implements OnInit {
  cardForm: FormGroup
  monthList = [];
  yearList = [];
  constructor(
    public dialogRef: MatDialogRef<AddCardComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private cardService: CardService
  ) {
    this.initialize()
  }

  ngOnInit(): void {
  }


  initialize() {
    this.cardForm = new FormGroup({
      cardNo: new FormControl('', {
        validators: [Validators.required],
      }
      ),
      cardType: new FormControl(''),
      month: new FormControl('', [Validators.required]),
      year: new FormControl('', [Validators.required]),
      cvv: new FormControl('',
        [Validators.required, Validators.minLength(3), Validators.maxLength(3)]
      ),
    })

    let year = (new Date()).getFullYear();
    this.yearList.push(year);
    for (let i = 1; i <= 12; i++) {
      this.monthList.push(i);
      this.yearList.push(year + i);
    }
    this.cardNoType();
    this.cardCvv();
  }

  cardNoType() {
    this.cardForm.get('cardNo').valueChanges.subscribe((value) => {
      this.cardValidation(value);
    })
  }

  cardCvv() {
    this.cardForm.get('cvv').valueChanges.subscribe((value) => {
      if (!value) return;
      if (value.length > 3) {
        let cvv = value.substr(0, 3);
        this.cardForm.get('cvv').setValue(cvv);
      }
    })
  }

  cardValidation(value) {
    if (!value) {
      this.cardForm.get('cardType').setValue('');
      return;
    }
    if (value && (value.startsWith('34') || value.startsWith('37'))) {
      this.cardForm.get('cardType').setValue('american');
    } else {
      this.cardForm.get('cardType').setValue('visa');
    }
  }

  checkCardDigit() {
    let cardNo = this.cardForm.value.cardNo.replace(/\s+/g, '');
    if (this.cardForm.value.cardType == 'american') {
      if (cardNo.length > 15) {
        cardNo = cardNo.substr(0, 15);
      }
      if (cardNo.length != 15) {
        this.cardService.showSnackBar('Card number should be 15 numbers');
        return false;
      }
    } else {
      if (cardNo.length > 16) {
        cardNo = cardNo.substr(0, 16);
      }
      if (cardNo.length != 16) {
        this.cardService.showSnackBar('Card number should be 16 numbers');
        return false;
      }
    }
    let card = this.data.cardList.find(ele => ele.cardNo == cardNo);
    if (card) {
      this.cardService.showSnackBar('Card already exsist');
      return false;
    }
    return cardNo;
  }

  checkCardExpiry() {
    let year = (new Date()).getFullYear();
    let month = (new Date()).getMonth();
    if (this.cardForm.value.year == year && this.cardForm.value.month < month) {
      this.cardService.showSnackBar('Card is expired. Please fill valid card details')
      return false;
    }
    return true;
  }
  
  saveData() {
    let cardNo = this.checkCardDigit();
    if (!cardNo || !this.checkCardExpiry()) return;

    let body = {
      cardNo,
      cardType: this.cardForm.value.cardType,
      month: this.cardForm.value.month,
      year: this.cardForm.value.year,
      cvv: this.cardForm.value.cvv
    }
    this.cardService.saveCardApi(body).subscribe((res) => {
      if (res && res['code'] == 'success') {
        this.data.cardList.push(body);
        this.cardService.showSnackBar('Card added successfully');
        this.dialogRef.close(true);
      }
    })
  }

  cancel() {
    this.dialogRef.close();
  }

}
